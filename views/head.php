  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>On Va Se La Mettrie - La Soirée Des Copains et des Copains des Copains.</title>
    <meta name="description" content="L'édition 2017 de On Va Se La Mettrie, aura lieu à La Mettrie, le samedi 26 Août 2017 à 14h">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <link rel="icon" type="image/png" href="../logo.png" />

    <link rel="stylesheet" href="../css/styles.css">
    <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
  </head>