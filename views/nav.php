<nav class="row">
  <div class="col-lg-1 col-md-3 col-sm-5 col-xs-1">
    <img class="logo" src="../img/logo.png" alt="">
  </div>
  <div class="col-lg-11 col-md-9 col-sm-7 col-xs-11">
    <div class="navbar-default navbar-right" role="navigation">
      <div class="navbar-header">   
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li> <a href="index.php">ACCUEIL</a> </li>
          <li> <a href="ambiance.php">L'AMBIANCE</a> </li>
          <li> <a href="programmation.php">PROGRAMMATION</a> </li>
          <li> <a href="biere.php">LA BIERE</a> </li>
          <li> <a href="trucs-pratiques.php">LES TRUCS PRATIQUES</a> </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
