<footer class="impair">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <table class="table">
          <tbody>
            <tr>
              <td><a href=#>A Propos</a></td>
              <td><a href=#>Contactez-nous</a></td>
            </tr>
            <tr>
              <td><a href=#>Partenaires</a></td>
              <td><a href=#>Retour</a></td>
            </tr>
            <tr>
              <td><a href=#>Qui sommes-nous</a></td>
            </tr>
            <tr><td><a href=#>Developpement</a></td></tr>
            <tr>
              <td><a href=#>Contactez-nous</a></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="col-lg-push-4 col-lg-4">
        <table class="table text-right">
          <tbody>
            <tr>
              <td>Fait à Saint-Denis par Fabian Rupin</td>
            </tr>
            <tr>
              <td>Copyright 2017</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</footer>