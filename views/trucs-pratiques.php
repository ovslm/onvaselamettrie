<!doctype html>
<html class="no-js" lang="">
  <?php include("head.php")?>
  <body id="homepage">
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Add your site or application content here -->

    <header id="couverture" class="impair">
      <div class="container">
        <?php include("nav.php")?>

        <div class="row">
          <div class="col-lg-5 col-md-7 col-sm-9 col-xs-11">
            <h1>ON VA SE LA METTRIE</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 col-md-7 col-sm-9 col-xs-12">
            <p>LA METTRIE - SAMEDI 26 AOUT 2017</p>
          </div>
        </div>
      </div>
    </header>

    <section class="homepage-section pair">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-push-2 text-justify">
            <h2>POUR VENIR</h2>
            <p>Malgré le fait que le lieu soit en campagne relativement assez profonde, l'adresse est disponible sur tout bon GPS : </p>
            <address>
              La Mettrie<br />
              35680 Bais<br />
              Téléphone : 02 99 96 11 63<br />
              Mobile : 06 82 11 32 34
            </address>
            <p>SI malgré ces informations votre GPS vous envoie en Mayenne, ou en Afrique du Sud, vous pouvez toujours utilisez ces coordonnées GPS : </p>
            <p>48°01'26.1"N 1°17'21.8"W</p>
            <div class="row">
              <div class="col-lg-12 text-center">
                <a href="description-festival.html" class="btn btn-primary">En savoir plus</a>
              </div>  
            </div>

            <figure class="img-section">
              <img src="../img/zcecile-de-france-swann.jpg" alt="cecile-de-france" class="img-full-width-section ">
            </figure>
          </div>
        </div>
      </div>
    </section>

    <section class="homepage-section impair">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-push-2 col-md-10 col-md-push-1 col-sm-12 text-justify">
            <h2>LES ACTUALITES DU FESTIVAL</h2>
            <p>Retrouvez toute l'actualité du festival, les prochains films, les avis des spectateurs, </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-12">
            <a href=article.html>
              <div class="rounded-image">
                <img src="../img/news/actu1.jpg" alt="panorama-rogne">
              </div>
              <h4 class="cut-title">L'actrice Roberta Clark en tournée pour son dernier film</h4>
              <p>Le réalisateur nous surprend encore une fois avec une oeuvre splendide et incroyablement envoutante.</p>
            </a>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-12">
            <a href=article.html>
              <div class="rounded-image">
                <img src="../img/romeo+juliette.jpg" alt="romeo+juliette-affiche">
              </div>
              <h4 class="cut-title">Déjà 1500 inscrits au festival !</h4>
              <p>Avec 3 séances déjà complètes, et plus de 20 inscriptions par jours, le festival risque de jouer à guichet fermé avant même le début du festival !</p>
            </a>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-12">
            <a href=article.html>
              <div class="rounded-image">
                <img src="../img/laroute-rogne.jpg" alt="laroute-rogne">
              </div>
              <h4 class="cut-title">Roby Macintosh, un monstre à lui tout seul</h4>
              <p>Quand l'acteur joue les gladiators dans le dernier film de Scotty Peppers, cela donne des clichés tout à fait épique.</p>
            </a>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-12">
            <a href=article.html>
              <div class="rounded-image">
                <img src="../img/colere-rogne.jpg" alt="colere-rogne">
              </div>
              <h4 class="cut-title">Monica Shcnedier standing ovation</h4>
              <p>L'actrice Monica shcnedier fait une standing ovation après son discours sur l'égalité hommes-femmes au cinéma.</p>
            </a>
          </div>

          <div class="col-lg-12 text-center">
            <a href="news.html" class="btn btn-primary">Retrouvez toutes les actualités du festival</a>
          </div>  

        </div>
      </div>
    </section>

    <section class="homepage-section pair">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-push-2 col-md-12 text-justify">
            <h2>LES PROCHAINES PROJECTIONS</h2>
            <p>En plus d'une cohérence, c'est une véritable expérience qui vous feront voyager de la naissance à l'adolescence, de la joie de l'aventure à la colère des puissants.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 col-lg-push-2">
            <ul class="nav nav-pills nav-justified">
              <li class="active"><a href="#panorama" data-toggle="tab">Panorama</a></li>
              <li><a href="#premier-amour" data-toggle="tab">Premier Amour</a></li>
              <li><a href="#la-route" data-toggle="tab">La Route</a></li>
              <li><a href="#colere-et-empire" data-toggle="tab">Colère et Empire</a></li>
            </ul>
          </div>
        </div>
        <div class="row tab-content">
          <div class="tab-pane active fade in" id="panorama">
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/lepapillon.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">LE PAPILLON<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/mud.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">MUD<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/septanstibet.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">7 ANS AU TIBET<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="premier-amour">
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/amour.jpeg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">AMOUR<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/romeo+juliette.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">ROMEO + JULIETTE<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/brockeback.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">BROKEBACK MOUNTAIN<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="la-route">
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/surlaroute.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">SUR LA ROUTE<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/littlemisssunshine.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">LITTLE MISS SUNSHINE<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/route2.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">LA ROUTE<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="colere-et-empire">
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/300.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">300<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/pompei.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">POMPEI<span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body" style="background-image:url(../img/gladiator.jpg)">
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-default" href="films.html">GLADIATOR <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php 
  include("footer.php");
  include("script.php");
    ?>
  </body>
</html>