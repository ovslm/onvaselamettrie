<!doctype html>
<html class="no-js" lang="">
  <?php include("head.php")?>
  <body>
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Add your site or application content here -->

    <?php include("header.php")?>

    <section id="description-festival" class="pair">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-push-2 text-justify">
            <h1>ON VA SE LA METTRIE</h1>
            <figure class="img-section">
              <img src="../img/DSC00815.JPG">
              <figcaption>2006 – La dernière photo connue de Gérard Majax, 36h après un de ses numéros fétiches, mais râtés "La peluche et le ballon".</figcaption>
            </figure>
            <h2>AVANT TOUT UNE HISTOIRE</h2>
            <p>C'est au cours d'une file d'attente d'un spectacle de marionnettes présentés par Gérard Majax au Puy du Fou que l'idée de faire une soirée avec des copains a été imaginée. C'est donc Gérard Majax lui-même qui a été l'invité d'honneur de la 1ère édition en 2006. Avant qu'il ne meurt bien sur et qu'il ne se fasse définitivement disparaitre, lui-même, dans un chapeau de cuir au cours d'une mauvaise manipulation de magie, lors d'une soirée bien arrosé avec des amis proches.</p>
            <p>Après ce tragique accident, le festival a été arrêté pendant près d'un an avant de reprendre comme d'habitude l'année d'après, parce que si Gérard était une célébrité très appérécié auprès de ses camarades de classe, de nombreux témoignagnes témoignait d'une certaine exaspération au sein de son cercle proche de l'académie française. Et après de nombreuses sous-entendu en interview, c'est Léonardo Di Caprio, son frère ami et amant qui lacha dans le Télégraphe "Non mais attendez, il est lourd. Je l'aime beaucoup mais il est lourd bordel ! La dernière fois qu'on a bouffé chez lui, il nous invite pour manger du pigeon. Très bien ! Moi j'adore le pigeon ! Sauf qu'a peine arriver à la table ce con les a changé patates douces. EN PATATES DOUCES PUTAIN ! C'est une chouette prouesse, d'accord, mais ça fait quand même chier quand tu bouffes ! Et pardonnez moi l'expression !"</p>
            <p>Alors c'est tout la jet-set qui s'amuse qui sera de l'édition mythique de 2010, ou 2001 (suite à une erreur de verre, personne n'est réellement capable de situer cette soirée), et marquera les esprits jusque dans les endroits les plus reculés du canton de Fougères-2. On se rappelera la présence de MartyMcFly (également présent pour une soirée en son hommage et son grand retour en 2015), d'Usain Bolt, de Marylin Monroe, Jacques Mayol, Perre Richard, Jean-Marc Barr et bien d'autres personnages du Grand Bleu.</p>
            <p>Année après année, le festival ne s'agrandit pas mais change de dates, du notamment au décalage du dernier samedi du mois d'Août, qui ne tombe malheureusement pas à la même date tous les ans, malgré les efforts de négociation des organisateurs. On relève à ce sujet une série d'anecdotes comme en 2013, ou la configuration du calendrier était particulièrement fourbe et ou 350 personnes sont venus la semaine d'après et ont malheureusement raté l'apéro. Leurs souhaits de rester sur place pour attendre l'édition suivante a posé quelques problèmes de logistiques, et ont notamment ralenti de façon significative les travaux de la LGV Grand Ouest (et semble-t-il l'EPR de Flamanville également).</p>
            <p>Ce problème serait également la cause d'erreurs de localisation. Ainsi en 2011 un des participants se serait retrouvé à Bais en Mayenne, et lors de son arrivée aurait crié "BAH OUAIP BAH OUAIP ÇA CHANGE DE DATE TOUT LE TEMPS AUSSI" laissant les organisateurs profondément déconcertés de cette situation inattendue.</p>

            <h2>LE DELIRE</h2>
            <p>Si la plage est assez éloignée et nécessite un véhicule pour s'y rendre, de nombreuses activités ludiques permettent de faire tourner les heures creuses, ainsi dans les meilleurs activités classé par le cercle du public sont les suivantes : </p>
            <ul>
              <li>Le Ventriglisse, activité lancée en 2010 (ou 2001) et arrêter la même année. Non pas que l'activité en question n'ait eu de succès torride, mais les participants se seraient plaints de marques de scarification avec l'insigne "DESERT" sur le torse après avoir tenter plusieurs fois l'expérience. Certains d'entre eux auraient même développés des troubles obscessionnelles compulsifs, criant parfois en plein Paris "DESERT ! DESERT ! PUTAIN MAIS REGARDE C'EST UN CAMION DE LA GUERCHE DE BRETAGNE ÇA !"</li>
              <li>Le Mandalah, expérience spirituelle tentée en 2015, qui malgré les doutes dans les coeurs du publics a rencontré un fort succès. Réalisé uniquement avec des végétaux et des minéraux présents sur le site, le mandala apporte sérénité et plenitude à tous les participants. Croyez le ou non, mais aucun des volontaires n'a vomi à cette soirée.</li>
              <li>Les jeux en bois.</li>
              <li>La Galette Saussice ou GS, expérience culinaire inconnu des franc-comtois, la galette saucisse reste pourtant la première cause d'immigration en Ille-et-Vilaine. En tout cas depuis que le championnat du monde de palet de La Guerche De Bretagne n'existe plus. Servie vers 21h ou 23h, ou entre les deux, fabriqué par un cochonnier de la région et accomagngé de galettes faite-main et directement sur le site, elle reste un des évènements phares et une marque de fabrique du festival.</li>
              <li>La bière, industrielle et fraîche, servi en demi et sans faux col. Si la plupart des participants semblent satisfaits, des épisodes de vomissement fulgurants ont poussé les organisateurs à se tourner vers une bière plus artisanale. Ainsi depuis 2015, un ami marin s'occupe du brassage à l'eau douce d'une partie de la consommation.</li>
              <li>Les concerts, malgré le lieu reculé et l'amateurisme des organisateurs, certains groupes acceptent d'occuper les lieux pour divertir les mélomanes.</li>
              <li>Et bien d'autres activités à découvrir...</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <?php 
  include("footer.php");
  include("script.php");
    ?>
  </body>
</html>
